import { TestBed } from '@angular/core/testing';

import { IngredientsDataService } from './ingredients-data.service';

describe('IngredientsDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IngredientsDataService = TestBed.get(IngredientsDataService);
    expect(service).toBeTruthy();
  });
});
