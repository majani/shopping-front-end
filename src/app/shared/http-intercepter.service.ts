import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class HttpIntercepterService implements HttpInterceptor {

  constructor(
    private authSvc: AuthService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler){
    let authHeaderString = this.authSvc.getToken();
    let userName = this.authSvc.getAuthenticateduser();

    if(authHeaderString && userName){
      req = req.clone({
        setHeaders: {
          Authorization: authHeaderString
        }
      })
    }

    return next.handle(req);
  }
}
