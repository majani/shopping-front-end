import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { User } from '../user/user.model';
import { Subject } from 'rxjs';
export const TOKEN = 'token';
export const AUTHENTICATED_USER = 'authenticatedUser';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userLoggedIn = new Subject<any>();

  constructor(
    private http: HttpClient
  ) { }

  authenticateUser(username: string, password: string) {
    return this.http.post<any>(`http://localhost:8080/authenticate`, { username, password }).pipe(
      map(
        data => {
          sessionStorage.setItem(AUTHENTICATED_USER, username);
          sessionStorage.setItem(TOKEN, `Bearer ${data.jwt}`);
          return data;
        }
      )
    )
  }


  registerUser(user:User){
    return this.http.post<User>('http://localhost:8080/register', user);
  }


  getAuthenticateduser() {
    return sessionStorage.getItem(AUTHENTICATED_USER);
  }

  getToken() {
    return sessionStorage.getItem(TOKEN);
  }

  isUserLoggedIn(){
    let user = sessionStorage.getItem(AUTHENTICATED_USER);
    this.userLoggedIn.next(AUTHENTICATED_USER);
    return !(user===null);
  }

  logOut(){
    sessionStorage.removeItem(AUTHENTICATED_USER);
    sessionStorage.removeItem(TOKEN);
    this.userLoggedIn.next(AUTHENTICATED_USER);
  }
}
