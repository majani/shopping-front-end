import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ingredient } from './ingredient.model';

@Injectable({
  providedIn: 'root'
})
export class IngredientsDataService {

  constructor(
    private http: HttpClient
  ) { }

  getIngredients(){
    return this.http.get<Ingredient[]>('http://localhost:8080/ingredients');
  }

  getIngredient(index:number){
    return this.http.get<Ingredient>(`http://localhost:8080/ingredients/${index}`);
  }

  updateIngredient(ingredient:Ingredient, index:number){
    return this.http.put(`http://localhost:8080/ingredients/${index}`, ingredient);
  }

  deleteIngredient(index:number){
    return this.http.delete(`http://localhost:8080/ingredients/${index}`)
  }
}
