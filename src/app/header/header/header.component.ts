import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isUserLoggedIn: boolean = false;

  constructor(
    private authSvc: AuthService
  ) { }

  ngOnInit() {
    //this.isUserLoggedIn = this.authSvc.isUserLoggedIn();
    this.authSvc.userLoggedIn.subscribe(
      data => {
        this.isUserLoggedIn = data;
      }
    )
  }

  

}
