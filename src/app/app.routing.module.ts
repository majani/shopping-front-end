import { NgModule } from "@angular/core";
import { Routes, RouterModule, PreloadAllModules} from "@angular/router"
import { RecipesComponent } from "./recipes/recipes.component";
import { ShoppingListComponent } from "./shopping-list/shopping-list.component";
import { RecipesStartComponent } from "./recipes/recipes-start/recipes-start.component";
import { RecipeDetailComponent } from "./recipes/recipe-detail/recipe-detail.component";
import { RecipeEditComponent } from "./recipes/recipe-edit/recipe-edit.component";
import { AuthComponent } from "./auth/auth.component";
import { UserComponent } from "./user/user.component";
import { LogoutComponent } from "./auth/logout/logout.component";
import { RouteGuardService } from "./shared/route-guard.service";

const appRoutes: Routes = [
    { path: '', redirectTo: '/auth', pathMatch: 'full' },
    { path: 'recipes', component: RecipesComponent, children: [
        { path: '', component: RecipesStartComponent },
        { path: 'new', component: RecipeEditComponent},
        { path: ':id', component:RecipeDetailComponent},
        { path: ':id/edit', component: RecipeEditComponent},
        
    ], canActivate: [RouteGuardService]},
    { path: 'shopping-list', component: ShoppingListComponent, canActivate: [RouteGuardService]},
    { path: 'auth', component:AuthComponent},
    { path: 'register', component:UserComponent},
    { path: 'logout', component: LogoutComponent, canActivate: [RouteGuardService]}
    
   
]
@NgModule({
    imports: [RouterModule.forRoot(appRoutes,{preloadingStrategy:PreloadAllModules})],
    exports: [RouterModule]
})
export class AppRoutingModule{

}