import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService, TOKEN, AUTHENTICATED_USER } from '../shared/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  isLogInMode = true;
  invalidLogin = true;
  isLoading = false;

  constructor(
    private authSvc: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }
 
  onSwitchMode(){
    this.isLogInMode = !this.isLogInMode;
  }

  onSubmit(form:NgForm){
    this.isLogInMode = true;
   this.authSvc.authenticateUser(form.value.username,form.value.password).subscribe(
    data => {
      //console.log(data);
      console.log(TOKEN)
      console.log(AUTHENTICATED_USER)
      this.invalidLogin = false;
      this.isLoading = false;
      this.router.navigate(['/recipes']);
    },
    error => {
      console.log(error);
      this.invalidLogin = true;
      this.isLoading = false;
    }
   
   )
  }

}
