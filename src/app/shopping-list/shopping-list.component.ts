import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppinglistService } from './shoppinglist.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit{

  ingredients: Ingredient[];
  private igChanged: Subscription;
  p:number = 1;

  constructor(
    private shoppingSvc: ShoppinglistService
  ) { }

  ngOnInit() {
    this.igChanged = this.shoppingSvc.ingredientsChanged.subscribe(
      data => {
        this.ingredients = data;
      }
    )
    this.shoppingSvc.getIngredients().subscribe(
      (ingredients: Ingredient[]) => {
        this.ingredients = ingredients;
      }
    )
  }


  ngOnDestroy() {
    this.igChanged.unsubscribe();
  }

  onEditItem(index: number) {
    this.shoppingSvc.startedEditing.next(index);
  }

}
