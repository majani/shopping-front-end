import { Injectable, OnInit } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ShoppinglistService {
  
  ingredientsChanged = new Subject<Ingredient[]>();
  startedEditing = new Subject<number>();
  ingredients : Ingredient[] = [];
  ingredient: Ingredient;

  constructor(
    private http : HttpClient
  ) { }

  

  getIngredients(){
    return this.http.get<Ingredient[]>('http://localhost:8080/ingredients');
  }

  addIngredient(ingredient: Ingredient){
    this.ingredients.push(ingredient);
    this.ingredientsChanged.next(this.ingredients.slice())

  }

  addIngredients(ingredients : Ingredient[]){
    this.ingredients.push(...ingredients);
    this.ingredientsChanged.next(this.ingredients.slice())

  }

  getIngredient(index: number){
    return this.http.get<Ingredient>(`http://localhost:8080/ingredients/${index}`);
  }

  updateIngredient(index: number, newIngredient:Ingredient){
    return this.http.patch(`http://localhost:8080/ingredients/${index}`, newIngredient);
  }
}
 