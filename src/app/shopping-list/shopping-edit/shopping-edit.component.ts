import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { ShoppinglistService } from '../shoppinglist.service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {

  subscription: Subscription;
  editMode: boolean = false;
  editedItemIndex: number;
  editedItem: Ingredient;

  // @ViewChild('nameInput', {static:false}) nameInputRef: ElementRef;
  // @ViewChild('amountInput', {static:false}) amountInputRef: ElementRef;
  @ViewChild('f', {static:true}) slForm: NgForm;




  constructor(
    private shoppingSvc: ShoppinglistService
  ) { }

  ngOnInit() {
    this.subscription = this.shoppingSvc.startedEditing.subscribe(
      (index: number) => {
        this.editMode = true;
        this.editedItemIndex = index;
        this.shoppingSvc.getIngredient(index).subscribe(
          response => {
            this.editedItem = response;
            this.slForm.setValue({
              name: this.editedItem.name,
              amount: this.editedItem.amount
            })
          }
        )

      }

    )

  }

  onAddItem(form: NgForm) {
    // const ingName = this.nameInputRef.nativeElement.value;
    // const ingAmount = this.amountInputRef.nativeElement.value;
    // const newIngredient = new Ingredient(ingName,ingAmount);
    // this.shoppingSvc.addIngredient(newIngredient); 

    const ingName = form.value.name;
    const ingAmount = form.value.amount;
    const newIngredient = new Ingredient(ingName, ingAmount);

    if (this.editMode) {
      this.shoppingSvc.updateIngredient(this.editedItemIndex, newIngredient).subscribe();
      this.shoppingSvc.getIngredients().subscribe(
        data => {
          this.shoppingSvc.ingredientsChanged.next(data);
          form.reset();
        }
      )

    }

    else {
      this.shoppingSvc.addIngredient(newIngredient);
    }


  }

  resetForm() {
    this.slForm.reset();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
