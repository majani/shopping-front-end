import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from './user.model';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  isLoading:boolean = false;

  constructor(
    private router:Router,
    private authSvc:AuthService
  ) { }

  ngOnInit() {
  }

  onSubmit(form:NgForm){
    this.isLoading = true;
    let newUser = new User(form.value.username, form.value.email,form.value.password);
    this.authSvc.registerUser(newUser).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['/auth']);
        this.isLoading = false;
      },
      error => {
        console.log(error);
        this.isLoading=false;
      }
    )


  }

  onCancel(){

  }

}
