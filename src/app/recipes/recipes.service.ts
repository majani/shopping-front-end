import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppinglistService } from '../shopping-list/shoppinglist.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {
  recipesChanged = new Subject<Recipe[]>();
  recipes: Recipe[] = [];

  constructor(
    private shoppingSvc: ShoppinglistService,
    private http: HttpClient
  ) { }


  //recipeSelected = new EventEmitter<Recipe>();
  recipeSelected = new Subject<Recipe>();


  // recipes: Recipe[] = [
  //   new Recipe("Kiymali Pide",
  //     "En lezzetli pide",
  //     "https://i1.wp.com/www.gastrosenses.com/wp-content/uploads/2014/12/pide-with-lamb-1024x680.jpg?resize=1024%2C680&quality=100&strip=all",
  //     [
  //       new Ingredient('Meat', 3),
  //       new Ingredient('French fries', 20),
  //       new Ingredient('Tomatoes', 3),
  //       new Ingredient('Spice', 10),
  //     ]),

  //   new Recipe("Lahmacun",
  //     "Tadina bak",
  //     "https://www.takeaway.com/foodwiki/uploads/sites/11/2019/08/lahmacun_3-1080x964.jpg",
  //     [
  //       new Ingredient('Meat', 3),
  //       new Ingredient('French fries', 20),
  //       new Ingredient('Tomatoes', 3),
  //       new Ingredient('Spice', 10),
  //     ]),


  //   new Recipe("Kaiseri Mantisi",
  //     "Hot and spicy",
  //     "https://recipes.timesofindia.com/recipes/turkish-manti-dumpling/photo/59969178.cms",
  //     [
  //       new Ingredient('Meat', 3),
  //       new Ingredient('Wrap', 20),
  //       new Ingredient('Tomatoes', 3),
  //       new Ingredient('Spice', 10),
  //     ]),


  //   new Recipe("Adana kebab",
  //     "Hot and spicy",
  //     "https://turkishfoodie.com/wp-content/uploads/2018/11/Adana-Kebab-.jpg",
  //     [
  //       new Ingredient('Meat', 3),
  //       new Ingredient('Wrap', 20),
  //       new Ingredient('Tomatoes', 3),
  //       new Ingredient('Spice', 10),
  //     ]),
  // ]

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice()); 
  }


  getRecipes() {
    return this.http.get<Recipe[]>('http://localhost:8080/recipes');
    //return this.recipes.slice();
  }

  getRecipe(index: number) {
    return this.http.get<Recipe>(`http://localhost:8080/recipes/${index}`);
    //return this.recipes[index];
  }

  addRecipe(recipe: Recipe) {
    return this.http.post('http://localhost:8080/recipes', recipe);
    //this.recipes.push(recipe);
    //this.recipesChanged.next(this.recipes.slice())

  }

  updateRecipe(newRecipe: Recipe, index: number) {
    return this.http.patch(`http://localhost:8080/recipes/${index}`, newRecipe)
    //this.recipes[index] = newRecipe;
    //this.recipesChanged.next(this.recipes.slice());

  }

  deleteRecipe(index: number) {
    return this.http.delete(`http://localhost:8080/recipes/${index}`);
    //this.recipes.splice(index, 1);
    //this.recipesChanged.next(this.recipes.slice())
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.shoppingSvc.addIngredients(ingredients);
  }
}
