import { Component, OnInit, Input } from '@angular/core';
import { Recipe } from '../recipe.model';
import { RecipesService } from '../recipes.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Ingredient } from 'src/app/shared/ingredient.model';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {

  recipe: Recipe;
  ingredients: Ingredient[] = [];
  imagePath: string = '';
  name: string = '';
  description: string = '';
  id: number;
  constructor(
    private recipeSvc: RecipesService,
    private route: ActivatedRoute,
    private svc: RecipesService,
    private router: Router
  ) { }

  //this.route.snapshot.params['id']
  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
      this.svc.getRecipe(this.id).subscribe(
        (recipe:Recipe) => {
          this.recipe = recipe;
          //this.recipe.ingredients = recipe.ingredients;
          this.ingredients = recipe.ingredients;
          console.log(this.recipe.ingredients);
        }
      )
      }
    )


  }

  onAddToShoppingList() {
    this.recipeSvc.addIngredientsToShoppingList(this.recipe.ingredients);
  }

  onEditRecipe() {
    //this.router.navigate(['../', this.id, 'edit'])
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

  deleteRecipe(){
    this.recipeSvc.deleteRecipe(this.id).subscribe()
    this.router.navigate(['/recipes'], {relativeTo:this.route});
  }

}
