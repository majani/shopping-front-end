import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { RecipesService } from '../recipes.service';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {

  id: number;
  editmode: boolean = false;
  recipeForm: FormGroup;
  recipes: Recipe[] = [];


  constructor(
    private route: ActivatedRoute,
    private recipeSvc: RecipesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.editmode = params['id'] != null;
        this.initForm();
      }
    );
  }

  private initForm() {

    let recipeName = '';
    let recipeImagePath = '';
    let recipeDescription = '';
    let recipeIngredients = new FormArray([]);

    if (this.editmode) {
      let recipe: Recipe;
      this.recipeSvc.getRecipe(this.id).subscribe(
        (result: Recipe) => {
          recipe = result;
          recipeName = recipe.name;
          recipeImagePath = recipe.imagePath;
          recipeDescription = recipe.description
          if (recipe.ingredients) {
            for (let ingredient of recipe.ingredients) {
              recipeIngredients.push(
                new FormGroup({
                  'name': new FormControl(ingredient.name, Validators.required),
                  'amount': new FormControl(ingredient.amount, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)])
                })
              )
            }
          }

          this.recipeForm = new FormGroup({
            'name': new FormControl(recipeName, Validators.required),
            'imagePath': new FormControl(recipeImagePath, Validators.required),
            'description': new FormControl(recipeDescription, Validators.required),
            'ingredients': recipeIngredients

          })
        }


      )

    }

    else {
      this.recipeForm = new FormGroup({
        'name': new FormControl(recipeName, Validators.required),
        'imagePath': new FormControl(recipeImagePath, Validators.required),
        'description': new FormControl(recipeDescription, Validators.required),
        'ingredients': recipeIngredients
    })

    }

}

  onSubmit() {
    const recipe = this.recipeForm.value
    if (this.editmode) {
      this.recipeSvc.updateRecipe(recipe, this.id).subscribe();
      this.router.navigate(['/recipes']);
    }

    else {
      this.recipeSvc.addRecipe(this.recipeForm.value).subscribe();
      this.router.navigate(['/recipes']);
    }

    this.recipeSvc.getRecipes().subscribe(
      data => {
        this.recipes = data;
        this.recipeSvc.recipesChanged.next(this.recipes);
      }
    )
  }

  onAddIngredient() {
    (<FormArray>this.recipeForm.get('ingredients')).push(
      new FormGroup({
        'name': new FormControl(null, Validators.required),
        'amount': new FormControl(null, [Validators.required, Validators.pattern((/^[1-9]+[0-9]*$/))])
      })
    )
  }

  onDeleteIngredient(i:number){
    (<FormArray>this.recipeForm.get('ingredients')).removeAt(i);

  }

  onCancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }



  get controls() {
    return (<FormArray>this.recipeForm.get('ingredients')).controls;
  }
}
